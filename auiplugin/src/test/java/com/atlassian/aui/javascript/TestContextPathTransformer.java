package com.atlassian.aui.javascript;

import com.atlassian.aui.spi.AuiIntegration;
import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

public class TestContextPathTransformer extends TestCase {
    private AuiIntegration auiIntegration;
    private ContextPathTransformer transformer;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        auiIntegration = mock(AuiIntegration.class);
    }

    public void testWithContext() throws Exception
    {
        mockProps("/jira");

        CharSequence result = transform("    var _context = \"%CONTEXT_PATH%\";");
        assertEquals("    var _context = \"\\/jira\";", result);
    }
    public void testEmptyContext() throws Exception
    {
        mockProps("");

        CharSequence result = transform("    var _context = \"%CONTEXT_PATH%\";");
        assertEquals("    var _context = \"\";", result);
    }

    private void mockProps(String base) throws Exception {
        stub(auiIntegration.getContextPath()).toReturn(base);
        transformer = new ContextPathTransformer(auiIntegration);
    }

    private String transform(String javascript) {
        SearchAndReplaceDownloadableResource resource =
                (SearchAndReplaceDownloadableResource) transformer.transform(null, null, null, null);
        return resource.transform(javascript).toString();
    }

}
