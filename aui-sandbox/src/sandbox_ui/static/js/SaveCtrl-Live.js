define("SaveCtrl", ["MainModule"], function(sandboxModule) {

	sandboxModule.controller("SaveCtrl", SaveCtrl);
	
	function SaveCtrl($scope, $http, saveService, $rootScope) {
		$scope.canSave = true;

		$scope.inlineDialog = AJS.InlineDialog(AJS.$("#publish-button"), 1, function(contents, trigger, showPopup){
	        contents.append(AJS.$("#url-inline-dialog"));
	        showPopup();
	    }, {arrowOffsetX: 30, noBind:true, width: 400});

		$scope.publish = function(){
			var js = $scope.editors.js.getValue(),
				css = $scope.editors.css.getValue(),
				html = $scope.editors.html.getValue();
			
			saveService.publishCode(js, css, html, function(newURL){
				$scope.changePublishURL(newURL);
				AJS.$("#url-inline-dialog").show();
				$scope.inlineDialog.show();
			});
		}

		var codeId = saveService.readURL();

		//Add code to detect if codeId is a component or not
		saveService.loadCodeFromServer(codeId, function(data, status, headers, config){
			$scope.setCode(data.html, data.js, data.css);
		});
	}
});