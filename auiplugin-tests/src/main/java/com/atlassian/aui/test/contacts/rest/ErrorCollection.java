package com.atlassian.aui.test.contacts.rest;

import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 */
@XmlRootElement
public class ErrorCollection
{

    @XmlElement (name = "errors")
    Map<String, String> errors;

    private ErrorCollection() {}

    public ErrorCollection(Map<String, String> errors)
    {
        this.errors = errors;
    }

}
